interface Response {
  token: string;
  user: {
    name: string;
    email: string;
  };
}

export function signIn(): Promise<Response> {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        token: '1a2b2c1t15y5hy4h1h15t5',
        user: {
          name: 'anderson',
          email: 'anderson@servidor.com',
        },
      });
    }, 2000);
  });
}
