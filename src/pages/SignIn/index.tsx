import React from 'react';
import {View, Button, StyleSheet} from 'react-native';
import {useAuth} from '../../contexts/auth';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});

const SignIn: React.FC = () => {
  const {signed, validar} = useAuth();

  const handleSigIng = () => {
    validar();
  };

  return (
    <View style={styles.container}>
      <Button title="Login" onPress={handleSigIng} />
    </View>
  );
};

export default SignIn;
