import React, {createContext, useState, useEffect, useContext} from 'react';
import {signIn} from '../services/auth';
import api from '../services/api';
import AsyncStorage from '@react-native-community/async-storage';

interface User {
  name: string;
  email: string;
}

interface AuthContextData {
  signed: Boolean;
  user: User | null;
  loading: boolean;
  validar(): Promise<void>;
  logout(): void;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

export const AuthProvider: React.FC = ({children}) => {
  const [user, setUser] = useState<User | null>(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const loadStorage = async () => {
      const storagedUser = await AsyncStorage.getItem('authrn-user');
      const storagedToken = await AsyncStorage.getItem('authrn-token');

      await new Promise((resolve) => setTimeout(resolve, 2000));

      if (storagedUser && storagedToken) {
        api.defaults.headers.Authorizaton = 'Bearer ' + storagedToken;
        setUser(JSON.parse(storagedUser));
      }

      setLoading(false);
    };

    loadStorage();
  }, []);

  const validar = async () => {
    const res = await signIn();

    api.defaults.headers.Authorizaton = 'Bearer ' + res.token;
    setUser(res.user);

    await AsyncStorage.setItem('authrn-user', JSON.stringify(res.user));
    await AsyncStorage.setItem('authrn-token', res.token);
  };

  const logout = () => {
    AsyncStorage.clear().then(() => {
      setUser(null);
    });
  };

  return (
    <AuthContext.Provider
      value={{signed: !!user, user, loading, validar, logout}}>
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth() {
  const context = useContext(AuthContext);
  return context;
}
